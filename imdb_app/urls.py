from django.urls import path

from . import views

app_name = "imdb_app"
urlpatterns = [
    path("", views.index, name="index"),
    path("content/", views.page1, name="detail"),
    path('vue_search_actor/', views.func_search_actor, name='func_search_actor')
]