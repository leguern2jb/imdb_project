let lastValueRemoved = "";
let lastValuesRemoved = [];

function ajouterValeur(inputListId, listInputId, errorTextId, emptyErrorTextId, successMessageId) {
  let inputList = document.getElementById(inputListId);
  let selectList = document.getElementById(listInputId);
  let errorText = document.getElementById(errorTextId);
  let emptyErrorText = document.getElementById(emptyErrorTextId);

  // Effacer les messages d'erreur précédents
  errorText.innerText = '';
  emptyErrorText.innerText = '';

  // Vérifier si l'input est vide
  if (inputList.value.trim() === '') {
    // Afficher le message d'erreur pour l'input vide
    emptyErrorText.innerText = 'Veuillez entrer une valeur.';
    return;
  }

  // Vérifier si la valeur n'est pas déjà dans la liste
  if (!isValueInList(selectList, inputList.value)) {
    let option = document.createElement('option');
    option.value = inputList.value;
    option.text = inputList.value;
    selectList.add(option);

    // Sélectionner automatiquement la nouvelle valeur ajoutée
    option.selected = true;

    // Effacer le champ d'entrée après l'ajout
    inputList.value = '';

    // Effacer le message d'erreur pour l'input vide s'il y en a un
    emptyErrorText.innerText = '';

    // Mettre à jour l'output de la liste
    updateListOutput();

    // Mettre à jour la dernière valeur supprimée (annuler la suppression)
    lastValueRemoved = undefined;

    // Affichage du message de succès avec la valeur ajoutée
    document.getElementById(successMessageId).innerText = 'La valeur "' + option.text + '" a bien été ajoutée.';
    document.getElementById(successMessageId).classList.remove("d-none");
    setTimeout(function () {
      document.getElementById(successMessageId).classList.add("d-none");
    }, 3000); // Masquer le message après 3 secondes
  } else {
    // Afficher le message d'erreur pour les doublons
    errorText.innerText = 'Cette valeur est déjà dans la liste.';
    // Effacer le message d'erreur pour l'input vide s'il y en a un
    emptyErrorText.innerText = '';
  }
}

function supprimerValeur(listInputId, deleteMessageId) {
  let selectList = document.getElementById(listInputId);
  let selectedIndex = selectList.selectedIndex;
  if (selectedIndex !== -1) {
    // Stocker la dernière valeur supprimée pour pouvoir l'annuler
    lastValuesRemoved.push(selectList.options[selectedIndex].text);
    selectList.remove(selectedIndex);

    // Mettre à jour l'output de la liste
    updateListOutput();

    // Affichage du message de suppression avec la valeur supprimée
    document.getElementById(deleteMessageId).innerText = 'La valeur "' + lastValuesRemoved[lastValuesRemoved.length - 1] + '" a été supprimée.';
    document.getElementById(deleteMessageId).classList.remove("d-none");
    setTimeout(function () {
      document.getElementById(deleteMessageId).classList.add("d-none");
    }, 3000); // Masquer le message après 3 secondes
  }
}

function annulerSuppression(listInputId, restoreMessageId) {
  let selectList = document.getElementById(listInputId);
  if (lastValuesRemoved.length > 0) {
    // Réinsérer la dernière valeur supprimée
    let lastValueRemoved = lastValuesRemoved.pop();
    let option = document.createElement('option');
    option.value = lastValueRemoved;
    option.text = lastValueRemoved;
    selectList.add(option);
    // Sélectionner l'option réinsérée
    option.selected = true;

    // Mettre à jour l'output de la liste
    updateListOutput();

    // Affichage du message de restauration avec la valeur restaurée
    document.getElementById(restoreMessageId).innerText = 'La valeur "' + option.text + '" a bien été restaurée.';
    document.getElementById(restoreMessageId).classList.remove("d-none");
    setTimeout(function () {
      document.getElementById(restoreMessageId).classList.add("d-none");
    }, 3000); // Masquer le message après 3 secondes
  }
}



function updateListOutput() {
  let outputListTab1 = document.getElementById('outputListTab1');
  let outputListTab2 = document.getElementById('outputListTab2');
  let selectList = document.getElementById('listInput');

  // Vérifier si la liste a au moins une option
  if (selectList.options.length > 0) {
    let selectedValue = selectList.options[selectList.selectedIndex].text;
    outputListTab1.innerText = 'Liste sélectionnée : ' + selectedValue;
    outputListTab2.innerText = 'Liste sélectionnée : ' + selectedValue;
  } else {
    // Si la liste est vide, afficher un message par défaut dans l'output
    outputListTab1.innerText = 'Aucune valeur sélectionnée';
    outputListTab2.innerText = 'Aucune valeur sélectionnée';
  }
}

function isValueInList(selectList, value) {
  for (let i = 0; i < selectList.options.length; i++) {
    if (selectList.options[i].value === value) {
      // La valeur est déjà dans la liste
      return true;
    }
  }
  // La valeur n'est pas dans la liste
  return false;
}

// Appeler la fonction au chargement de la page pour afficher la première valeur par défaut
document.addEventListener('DOMContentLoaded', function () {
  updateListOutput();
});

// Ajoutez cet événement pour détecter le changement dans la liste
document.getElementById('listInput').addEventListener('change', function () {
  updateListOutput();
});

// Ajoutez cet événement pour détecter le changement dans la liste
document.getElementById('listInputTab1').addEventListener('change', function () {
  updateListOutput();
});

function func_search_actor() {
  // Récupérer la valeur de inputOnglet1 depuis la page HTML
  let valeurInputOnglet1 = document.getElementById('inputOnglet1').value;

  // Effectuer une requête AJAX avec la valeur de inputOnglet1
  fetch('/app/vue_search_actor/', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'X-CSRFToken': getCookie('csrftoken'),
    },
    // Convertir l'objet à envoyer en JSON
    body: JSON.stringify({ input_value: valeurInputOnglet1 }),
  })
    .then(response => {
      // Afficher le contenu de la réponse dans la console
      console.log('Contenu de la réponse :', response);

      // Vérifier le statut de la réponse
      if (!response.ok) {
        throw new Error('Erreur de réseau.');
      }

      // Parser la réponse JSON
      return response.json();
    })
    .then(data => {
      // Afficher le contenu de data dans la console
      console.log('Contenu de data :', data);

      // Traiter le résultat ici
      let result_name = data.name;
      let result_url_img = data.url_img;
      console.log('Résultat de la fonction Python :', result_name);

      // Mettre à jour la page avec le résultat si nécessaire
      document.getElementById('textOutputTab1').value = result_name;
      document.getElementById('imageOutputTab1').src = result_url_img;
      document.getElementById('imageOutputTab1').style.height = "250px";
    })
    .catch(error => console.error('Erreur lors de la requête AJAX :', error));
}

// Fonction pour obtenir le jeton CSRF depuis les cookies
function getCookie(name) {
  let cookieValue = null;
  if (document.cookie && document.cookie !== '') {
    let cookies = document.cookie.split(';');
    for (let cookie of cookies) {
      if (cookie.substring(0, name.length + 1) === (name + '=')) {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }
  return cookieValue;
}