from django.http import HttpResponse, JsonResponse
from django.shortcuts import render

from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

from imdb import Cinemagoer
import json

ia = Cinemagoer()

def index(request):
    return render(request, "imdb_app/index.html")

def page1(request):
    return render(request, "imdb_app/page1.html")

@require_POST
@csrf_exempt
def func_search_actor(request):
    # Récupérez les données POST depuis la requête
    data = json.loads(request.body.decode('utf-8'))

    # Récupérez la valeur de l'inputOnglet1 depuis les données POST
    input_name = data.get('input_value', '')

    search_person = ia.search_person(input_name)[0]

    search_person_data = search_person.__getattribute__("data") if search_person is not None else None

    name = search_person_data.get("name") if search_person is not None else None
    url_img = search_person_data.get("headshot") if search_person is not None else None

    # Retournez le résultat au format JSON
    return JsonResponse({'name': name, "url_img": url_img})